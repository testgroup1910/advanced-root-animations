using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class MoveNoRootMotion : MonoBehaviour
{
    CharacterController c;
    Vector3 movement = Vector3.zero;
    float speed = 10f;

    CinemachineVirtualCameraBase vCam;

    Animator animator;

    void Start()
    {
        c = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        vCam = Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CinemachineVirtualCameraBase>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.z = Input.GetAxis("Vertical");
        movement *= 0.5f;
        if (Input.GetKey(KeyCode.LeftShift))
            movement *= 2f;
    }

    private void FixedUpdate()
    {
        animator.SetFloat("speed", movement.z, 0.05f, Time.fixedDeltaTime);
        transform.rotation = Quaternion.Euler(0,vCam.transform.rotation.eulerAngles.y,0);   
    }
    private void OnAnimatorMove()
    {
        c.Move(animator.deltaPosition);
    }
}
